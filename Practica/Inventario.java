//Sofía Gutiérrez Mora C03632
import javax.swing.JOptionPane;

public class Inventario
{
public static void main (String args[])
	{
	String placa,marca,modelo,tipo,accesorios;
	boolean estado=true;
	char opcion;
	
	ArchivoEquipos archivos = new ArchivoEquipos();
	Equipo listaEquipos[] = new Equipo[archivos.length()];
	
	
	do 
		{
		String opcionString = JOptionPane.showInputDialog("Seleccione una opcion:\na. Registrar datos de los equipos.\nb. Mostrar la información de los equipos.\nc. Salir");
		opcion = opcionString.charAt(0);

		switch(opcion)
				{
				case 'a':
				for (int indice = 0; indice < listaEquipos.length; indice++){
					Equipo equipo = new Equipo();
					
					equipo.setPlaca(archivos.getPlaca(indice));
					equipo.setMarca(archivos.getMarca(indice));
					equipo.setModelo(archivos.getModelo(indice));
					equipo.setTipo(archivos.getTipo(indice));
					equipo.setAccesorios(archivos.getAccesorios(indice));
					equipo.setEstado(archivos.getEstado(indice));
					
					listaEquipos[indice] = equipo;
				}//fin del for
				JOptionPane.showMessageDialog(null, "El registro termino con exito");
				break;
				
				case 'b':
					String salida = " ";
					for (int indice = 0; indice < archivos.length(); indice++){
						salida+="  "+listaEquipos[indice].toString()+"\n";
						
					}//fin del for
					JOptionPane.showMessageDialog(null, salida);
				break;
				
				case 'c':
				
				break;
			}
		}while(opcion != 6);//fin del while
	}//fin del main
}//fin de la class
