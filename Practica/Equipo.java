//Sofía Gutiérrez Mora C03632
public class Equipo
{
	private String placa;
	private String marca;
	private String modelo;
	private String tipo;
	private String accesorios;
	private boolean estado;
	public Equipo(){}
	
	public Equipo(String placa, String marca, String modelo, String tipo, String accesorios, boolean estado)
	{
		setPlaca(placa);
		setMarca(marca);
		setModelo(modelo);
		setTipo(tipo);
		setAccesorios(accesorios);
		setEstado(estado);
	}
	
	public void setPlaca(String placa)
	{
		this.placa = placa;
	}
	public String getPlaca()
	{
		return placa;
	}
	public void setMarca(String marca)
	{
		this.marca = marca;
	}
	public String getMarca()
	{
		return marca;
	}
	public void setModelo(String modelo)
	{
		this.modelo = modelo;
	}
	public String getModelo()
	{
		return modelo;
	}
	public void setTipo(String tipo)
	{
		this.tipo = tipo;
	}
	public String getTipo()
	{
		return tipo;
	}
	public void setAccesorios(String accesorios)
	{
		this.accesorios = accesorios;
	}
	public String getAccesorios()
	{
		return accesorios;
	}
	public void setEstado(boolean estado)
	{
		this.estado = estado;
	}
	public boolean getEstado()
	{
		return estado;
	}
	public String toString()
	{
		return "Placa: "+getPlaca() + "Marca: "+getMarca() + "Modelo: "+getModelo() + "Tipo: "+getTipo() + "Accesorios: "+getAccesorios() + "Estado: "+getEstado();
	}
	
}
