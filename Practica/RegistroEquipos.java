//Sofía Gutiérrez Mora C03632
public class RegistroEquipos
{
	private Equipo equipos[];//a) Un atributo privado llamado equipos y será de tipo arreglo de Equipo.
	
	public RegistroEquipos(){//b) Un método constructor que no recibe parámetros y crea un arreglo de 4 equipos.
		equipos = new Equipo[4];
	}//fin del constructor sin parametros
	
	public RegistroEquipos(int longitud){//c) Un método constructor que recibe un valor de tipo entero y crea un arreglo de esa cantidad de equipos
		equipos = new Equipo[longitud];
	}//fin del constructor con árametros
	
	public void setEquipo(int indice, Equipo  equipo){//d) Un método setEquipo que recibe un valor de tipo entero (indice) y un objeto de tipo Equipo y lo asigna a la posición que se solicita en el índice
		equipos[indice] = equipo;
	}//fin del setEquipo
	
	public Equipo  getEquipo(int indice){//e) Un método getEquipo que recibe un valor de tipo entero (indice) y retorna el objeto contenido en esa posición.
		return equipos[indice];
	}//fin del getEquipo
	
	public int length(){//f) Un método llamado length que retorna el tamaño del arreglo.
		return equipos.length;
	}//fin length
	
	public int getPosicion(String placa){//g) Un método que retorna la posición que ocupa un equipo. Este método hará la búsqueda por medio del número de placa.
		
		int newIndice = 0;
		for(int indice=0; indice < equipos.length;indice++)
		{
			if(equipos[indice].getPlaca().equals(placa))
			{
				newIndice = indice;
			}//fin del if
		}//Fin del for
		return newIndice;
	}//fin get posicion
	
	public String getEquipoTipo(String tipo){//Un método llamado getEquipoTipo que recibe un parámetro de tipo String correspondientea un tipo y retorna en un String los equipos del tipo solicitado.
		String salida= "Los equipos de tipo son: \n\n";
		for (int indice =0;indice < equipos.length; indice++)
		{
			if(equipos[indice].getTipo().equals(tipo))
			{
				salida+=" - "+equipos[indice].toString()+"\n";
			}//fin del if
		}//fin del for
		return salida;
	}//Fin del getEquipo
	
	public void addAccesorio(String accesorio,String placa){//i) Un método que agrega un nuevo accesorio del equipo. Recibe un valor de tipo String que corresponde al nuevo accesorio.
		 
	}//Fin del addAccesorios
		
	public void removerEquipo(String placa){
	}//fin del removerEquipo
	
}//fin de la clase
